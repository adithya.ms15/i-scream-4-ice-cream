import numpy.random as rnd

class Day():
    def __init__(self):
        self.day = 1
        self.phase = 0
        self.temperature = 20

        self.wanted_ice = 0
        self.wanted_cones = 0
        self.wanted_sprinkles = 0

        self.price = 0
        
        self.last_sold = 0
        self.last_earned = 0
        
        self.weather = "Sunny"
        self.all_weather = ['Sunny', 'Cloudy', 'Rainy', 'Snowy']
        self.weather_cost = 3

        self.inventory = Inventory()

    def add_ent(self, ty):
        if ty == 'ice':
            self.wanted_ice += 10
        if ty == 'cone':
            self.wanted_cones += 10
        if ty == 'sprinkle':
            self.wanted_sprinkles += 10

    def set_price(self, price):
        self.price = price

    def determine_sales(self):
        sales_max = 10 * self.weather_cost * (self.temperature/10) * (
                    1 * (self.weather_cost + 1) - self.price) * (
                    min(1.2, (1 + self.inventory.inv_sprinkles / 1000)))
        if sales_max < 0:
            sales_max = 0
        return int(min(self.inventory.inv_ice, self.inventory.inv_cones_tot, sales_max))

    def determine_temp(self):
        delta_temperature = rnd.normal(0, 2.5)
        if delta_temperature > 5:
            delta_temperature = 5
        if delta_temperature < -5:
            delta_temperature = -5

        self.temperature += delta_temperature
        if self.temperature > 40:
            self.temperature = 40
        if self.temperature < -20:
            self.temperature = -20
            
    def update_weather_keys(self):
        if self.weather == 'Snowy':
            self.weather_cost = 0
        if self.weather == 'Rainy':
            self.weather_cost = 1
        if self.weather == 'Cloudy':
            self.weather_cost = 2
        if self.weather not in ["Snowy", "Rainy", "Cloudy"]:
            self.weather_cost = 3    

    def determine_weather(self):
        self.weather = self.all_weather[rnd.randint(0, 3)]
        self.update_weather_keys()

    def continue_day(self):
        if self.phase == 0:
            self.phase = 1
            self.inventory.order_ice(self.wanted_ice)
            self.wanted_ice = 0

            self.inventory.order_cones(self.wanted_cones)
            self.wanted_cones = 0

            self.inventory.order_sprinkles(self.wanted_sprinkles)
            self.wanted_sprinkles = 0

        elif self.phase == 1 and self.price > 0:
            sales = self.determine_sales()
            self.last_sold = sales

            self.inventory.inv_sprinkles -= sales
            if self.inventory.inv_sprinkles < 0:
                self.inventory.inv_sprinkles = 0
            self.inventory.inv_ice -= sales
            
            if self.inventory.inv_cones[0] > 0:
                self.inventory.inv_cones[0] -= sales
            elif self.inventory.inv_cones[1] > 0:
                self.inventory.inv_cones[1] -= sales
            else:
                self.inventory.inv_cones[2] -= sales

            self.inventory.balance += sales * self.price
            self.last_earned = sales * self.price

            self.phase = 0
            self.day += 1
            self.determine_temp()
            self.determine_weather()
            self.inventory.update_inventory()
            self.inventory.update_costs()


class Inventory():
    def __init__(self):
        
        self.balance = 1000

        self.cost_ice = 0.30
        self.cost_cones = 0.15 
        self.cost_sprinkles = 0.07

        self.inv_ice = 0
        self.inv_cones = [0, 0, 0]
        self.inv_cones_tot = 0
        self.inv_sprinkles = 0
        
    def verify_costs(self):
        if self.cost_ice <= 0 or self.cost_ice >= 1.0:
            self.cost_ice = 0.3
        if self.cost_cones <= 0 or self.cost_cones >= 0.5:
            self.cost_cones = 0.15
        if self.cost_sprinkles <= 0 or self.cost_sprinkles >= 0.2:
            self.cost_sprinkles = 0.07

    def update_costs(self):

        coinflips = rnd.uniform(0, 1, 3)

        if coinflips[0] >= 0.66:
            self.cost_ice += 0.03
        if coinflips[0] < 0.33:
            self.cost_ice -= 0.03

        if coinflips[1] >= 0.66:
            self.cost_cones += 0.02
        if coinflips[1] < 0.33:
            self.cost_cones -= 0.02

        if coinflips[2] >= 0.66:
            self.cost_sprinkles += 0.01
        if coinflips[2] < 0.33:
            self.cost_sprinkles -= 0.01

        self.verify_costs()

    def order_ice(self, amount):
        self.inv_ice += amount
        self.balance -= amount*self.cost_ice
    
    #function that processes the ordering of a number of cones "amount"
    #this is done by updating inventory and balance variables
    def order_cones(self, amount):
        #initial buying of cones are fresh and enter the 3rd category
        self.inv_cones[2] += amount
        #total number of cones in the inventory is greater by the same amount
        self.inv_cones_tot += amount
        #balance is updated to account for costs of buying these cones
        self.balance -= amount*self.cost_cones
     
    
    def order_sprinkles(self, amount):
        self.inv_sprinkles += amount
        self.balance -= amount*self.cost_sprinkles

        
    #function that updates the inventory by removing ice that has gone bad,
    #cones can survive for up to three days and run through the categories
    def update_inventory(self):
        
        self.inv_ice = 0
        self.inv_cones[0] = self.inv_cones[1]
        self.inv_cones[1] = self.inv_cones[2]
        self.inv_cones[2] = 0
        self.inv_cones_tot = sum(self.inv_cones)
