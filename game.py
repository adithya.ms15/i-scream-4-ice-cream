import pygame
import sys
from pygame.locals import *
from ice_cream import Day


class DisplayGame():
    def __init__(self):
        pygame.init()
        self.game = Day()

        self.width = 1200
        self.height = 900

        self.display_surface = pygame.display.set_mode((self.width, self.height))
        self.FPS = pygame.time.Clock()
        self.FPS.tick(60)

        self.bg = pygame.image.load("assets/bg.jpeg")

        self.bg = pygame.transform.scale(self.bg, (self.width, self.height))

        pygame.display.set_caption("Ice SCREAM")

        self.bigfont = pygame.font.Font('freesansbold.ttf', 64)
        self.smallfont = pygame.font.Font('freesansbold.ttf', 32)

        self.start_game()

    def display_state(self):
        day_text = self.bigfont.render(f'Day {self.game.day}',
                                       True, pygame.Color(0, 0, 0))
        day_rect = day_text.get_rect()
        day_rect.center = (self.width / 2, self.height * 5 / 8)

        temp_text = self.bigfont.render(f'Temp: {round(self.game.temperature, 2)}'
                                        f' C',
                                        True, pygame.Color(0, 0, 0))
        temp_rect = temp_text.get_rect()
        temp_rect.center = (self.width / 2, self.height * 6 / 8)

        if self.game.phase == 0:
            next_text = self.bigfont.render(f'Press "n" to buy stock!',
                                            True, pygame.Color(120, 0, 0))
        if self.game.phase == 1:
            next_text = self.bigfont.render(f'Press "n" to go sell!',
                                            True, pygame.Color(120, 0, 0))
        next_rect = next_text.get_rect()
        next_rect.center = (self.width / 2, self.height * 7 / 8)

        prices_text = self.smallfont.render(f'PRICES... I: €{round(self.game.inventory.cost_ice, 2)} --'
                                       f'C: €{round(self.game.inventory.cost_cones, 2)} --'
                                       f'S: €{round(self.game.inventory.cost_sprinkles, 2)}',
                                            True, pygame.Color(0,0,0))
        prices_rect = prices_text.get_rect()
        prices_rect.center = (self.width / 2, self.height * 3 / 8)

        ice_text = self.smallfont.render(f'New_ices: {self.game.wanted_ice}', True, pygame.Color(0, 0, 0))
        ice_rect = day_text.get_rect()
        ice_rect.center = (self.width / 4, self.height * 2 / 4)

        cone_text = self.smallfont.render(f'New_cones: {self.game.wanted_cones}', True, pygame.Color(0, 0, 0))
        cone_rect = day_text.get_rect()
        cone_rect.center = (self.width / 2, self.height * 2 / 4)

        sprinkles_text = self.smallfont.render(f'New_sprinkles: {self.game.wanted_sprinkles}', True, pygame.Color(0, 0, 0))
        sprinkles_rect = day_text.get_rect()
        sprinkles_rect.center = (self.width * 3 / 4, self.height * 2 / 4)

        price_text = self.bigfont.render(f'Price €{self.game.price}',
                                           True, pygame.Color(0,0,0))
        price_rect = price_text.get_rect()
        price_rect.center = (self.width * 4 / 8, self.height * 3 / 8)

        sold_text = self.smallfont.render(f'SOLD: {self.game.last_sold} '
                                          f'FOR €{self.game.last_earned}!!!',
                                          True, pygame.Color(0, 0,0))
        sold_rect = sold_text.get_rect()
        sold_rect.center = (self.width * 2 / 4, self.height * 1 / 4)

        inventory_text = self.smallfont.render(
            f'Ices: {self.game.inventory.inv_ice} -- '
            f'Cones: {self.game.inventory.inv_cones_tot} -- '
            f'Sprinkles: {self.game.inventory.inv_sprinkles} -- '
            f'Money: €{round(self.game.inventory.balance, 2)}', True,
            pygame.Color(0, 0, 0))

        inventory_rect = day_text.get_rect()
        inventory_rect.center = (self.width * 1 / 4, self.height * 1 / 8)

        self.display_surface.blit(day_text, day_rect)
        self.display_surface.blit(temp_text, temp_rect)
        self.display_surface.blit(next_text, next_rect)

        if self.game.phase == 0:
            if self.game.day > 1:
                self.display_surface.blit(sold_text, sold_rect)
            self.display_surface.blit(ice_text, ice_rect)
            self.display_surface.blit(cone_text, cone_rect)
            self.display_surface.blit(sprinkles_text, sprinkles_rect)
            self.display_surface.blit(prices_text, prices_rect)
        else:
            self.display_surface.blit(price_text, price_rect)

        self.display_surface.blit(inventory_text, inventory_rect)

    def start_game(self):
        while True:
            self.display_surface.blit(self.bg, (0, 0))
            self.display_state()
            pressed_keys = pygame.key.get_pressed()

            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                if pressed_keys[K_n]:
                    self.game.continue_day()

                if pressed_keys[K_i]:
                    self.game.add_ent("ice")

                if pressed_keys[K_c]:
                    self.game.add_ent("cone")

                if pressed_keys[K_s]:
                    self.game.add_ent("sprinkle")

                if pressed_keys[K_UP]:
                    self.game.set_price(self.game.price + 0.50)

                if pressed_keys[K_DOWN]:
                    self.game.set_price(max(self.game.price - 0.50, 0))

            pygame.display.update()


if __name__ == "__main__":
    game = DisplayGame()
